# 417_SniPCAP #

 This application captures Ethernet packages in the traffic in promiscuous
 mode, store them to memory until the user download it from the ftp client
 
 To view article on this product visit www.netburner.com/learn/ or 
 http://acsweb.ucsd.edu/~bokang/blog.html

### Hardware ###
[NetBurner MOD5441X development kit](http://www.netburner.com/products/core-modules/mod5441x#kit)

### Version Log ###

 1.0 - 6/26/2015

   + added filter: if dest of the package is set to MY_MAC_ADDR
                  do nothing with it
   + added web ui: start,stop,rest,download button, and functionality)
   + changed name SnaPCAP -> SniPCAP
   + download fixed - no longer take user straight to ftp

 1.1 - 6/30/2015

   + structure fixes
   + main organized, cleaned

 1.2 - 7/6/2015

   + changed libraries for hardware switch enable;
   + EnablePromiscuous() added
   + license added


### Notes ###

417_SniPCAP copyright (c) 2015 by

ByoungOh Kang (http://acsweb.ucsd.edu/~bokang)

and NetBurner, Inc. (http://www.netburner.com/)

Distributed under the BSD 3-Clause License

See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause