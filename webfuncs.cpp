/*                  */
/*                                        */
/*******************************************************************************
 *
 * Copyright 1998-2012 NetBurner, Inc.  ALL RIGHTS RESERVED
 *
 * Permission is hereby granted to purchasers of NetBurner hardware to use or
 * modify this computer program for any use as long as the resultant program is
 * only executed on NetBurner-provided hardware.
 *
 * No other rights to use this program or its derivatives in part or in whole
 * are granted.
 *
 * It may be possible to license this or other NetBurner software for use on
 * non-NetBurner hardware.  Please contact licensing@netburner.com for more
 * information.
 *
 * NetBurner makes no representation or warranties with respect to the
 * performance of this computer program, and specifically disclaims any
 * responsibility for any damages, special or consequential, connected with use
 * of this program.
 *
 * ----------------------------------------------------------------------------
 *
 * NetBurner, Inc.
 * 5405 Morehouse Drive
 * San Diego, CA  92121
 *
 * Information available at:  http://www.netburner.com
 * Support available at:      http://support.netburner.com
 *
 ******************************************************************************/
/*------------------------------------------------------------------------------
 * File:         webfuncs.cpp
 * Description:  Dynamic web server content functions.
 *----------------------------------------------------------------------------*/

#include "predef.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <startnet.h>
#include <autoupdate.h>
#include <dhcpclient.h>
#include <tcp.h>
#include <udp.h>
#include <netrx.h>
#include "main.h"

extern "C" {
   void ProcessUserRequest( int sock, PCSTR url );
}

extern int captureFull; // if(capBuffer full)?1:0

void ProcessUserRequest( int sock, PCSTR url )
{
    char buffer[100];

    // making buttons htm
    siprintf(buffer, "<a href=\"/INDEX.HTM?1\" target=\"_parent\"><button>Start</button></a>");
    writestring(sock, buffer);
    siprintf(buffer, "<a href=\"/INDEX.HTM?2\" target=\"_parent\"><button>Pause</button></a>");
    writestring(sock, buffer);
    siprintf(buffer, "<a href=\"/INDEX.HTM?3\" target=\"_parent\"><button>Reset</button></a>");
    writestring(sock, buffer);
    siprintf(buffer, "<a href=\"/SAMPLE.PCAP\" target=\"_parent\"><button>Download</button></a>");
    writestring(sock, buffer);

    int request;
    if ( *(url + 9) == '?' ) {
        request = atoi ( (char *) (url + 10) );
    }
    else {
        request = 0;
    }

    switch (request) {
        case 1: StartCapture();
                break;
        case 2: PauseCapture();
                break;
        case 3: ResetCapture();
                break;
    }
}
