/*                      */
/******************************************************************************
 *
 * Copyright 2007 NetBurner, Inc.  ALL RIGHTS RESERVED
 *   Permission is hereby granted to purchasers of NetBurner Hardware
 *   to use or modify this computer program for any use as long as the
 *   resultant program is only executed on NetBurner provided hardware.
 *
 *   No other rights to use this program or it's derivitives in part or
 *   in whole are granted.
 *
 *   It may be possible to license this or other NetBurner software for
 *   use on non NetBurner Hardware. Please contact Licensing@Netburner.com
 *   for more information.
 *
 *   NetBurner makes no representation or warranties with respect to the
 *   performance of this computer program, and specifically disclaims any
 *   responsibility for any damages, special or consequential, connected
 *   with the use of this program.
 *
 *   NetBurner, Inc
 *   5405 Morehouse Drive
 *   San Diego Ca, 92121
 *   http://www.netburner.com
 *
 *****************************************************************************/

/******************************************************************************
 417_SniPCAP_vr1.1

 This application captures Ethernet packages in the traffic in promiscuous
 mode, store them to memory until the user download it from the ftp client

 Version Log
 1.0 - 6/26/2015
 + added filter: if dest of the package is set to MY_MAC_ADDR
                  do nothing with it
 + added web ui: start,stop,rest,download button, and functionality)
 + changed name SnaPCAP -> SniPCAP
 + download fixed - no longer take user straight to ftp

 1.1 - 6/30/2015
 + structure fixes
 - main organized, cleaned

 // TODO : worry about actual size date and etc.
 *****************************************************************************/

#include "predef.h"
#include <stdio.h>
#include <stdlib.h>
#include <ucos.h>
#include <autoupdate.h>
#include <string.h>
#include <taskmon.h>
#include <networkdebug.h>
#include <startnet.h>
// netDoRX includes
#include <netrx.h>
#include <nettypes.h>
#include <utils.h>
#include <sim.h>
#include <ethervars.h>
// ftp includes
#include <ftpd.h>
#include <init.h>
#include <fdprintf.h>
#include <basictypes.h>        // Include for variable types
#include <iosys.h>             // Include for write, writeall
#include <netinterface.h>      // Include for InterfaceBlock
#include "nbprintfinternal.h"

#include <http.h>
#include <htmlfiles.h>
typedef int ( http_gethandler )( int sock, PSTR url, PSTR rxb );

extern "C" {
    void UserMain(void * pd);
}

const char * AppName="417_SniPCAP_vr1.0";
#define READ_FILENAME "SAMPLE.PCAP"
#define CAP_BUFF_SIZE 50000000 // 50 MB Maximum store size
#define HEADER_SIZE 24
typedef int (*netDoRXFunc)(PoolPtr, WORD, int);

struct packet {
    DWORD lenPdata;
    DWORD time;
    DWORD timeFrac;
    BYTE pData[];
} __attribute__( ( packed ) );

packet * packetToStore;
BYTE capBuffer[CAP_BUFF_SIZE];
uint32_t bufferIndex = 0;
uint32_t totalP = 0;

int captureFull = 0; // if(capBuffer full)?1:0

struct fileHeader {
    DWORD MAGIC;
    WORD MAJORVR;
    WORD MINORVR;
    DWORD THISZONE;
    DWORD SIGFIGS;
    DWORD SNAPLEN;
    DWORD NETWORK;
} __attribute__( ( packed ) );

// Populate file header
fileHeader header = {0xa1b2c3d4, 0x0002, 0x0004, 0x00000000, 0x00000000 , 0x00400000, 0x00000001};



/*
 * Function: myNetDoRX
 * -------------------
 * stores each incoming packet to capBuffer storing
 * length, time, timeFrac, and the data respectively
 *
 * pp : pointer to get buffer
 * w  : size of pp.pData[]
 * if_num : not used in this application
 */
int myNetDoRX(PoolPtr pp, WORD w, int if_num) {
    InterfaceBlock *pifb = GetInterFaceBlock();

    // filter dest == MyMac
    if((memcmp(&(((EFRAME*)pp->pData) -> dest_addr),
           &pifb->theMac, sizeof(&pifb->theMac))) == 0) {
	    return 0;
    }

    // prevent overflow
    if ((bufferIndex + w) > CAP_BUFF_SIZE) {
       	captureFull = 1;
    	iprintf("\r\n buffer is full: PAUSING \r\n");
        ClearCustomNetDoRX();
        return 0;
    }

    // location current packet placement
    packetToStore = (packet*) (capBuffer + bufferIndex);

    // populating packet data
    packetToStore->lenPdata = w;
    packetToStore->time = pp->dwTime;
    packetToStore->timeFrac = pp->dwTimeFraction;
    memcpy(packetToStore->pData, pp->pData, w);

    bufferIndex += w + sizeof(packet);
    totalP += 1;

    iprintf(".");

    return 0;
}

/*
 * Function: ResetCapture
 * ----------------------
 * readies application to rewrite the capture file.
 */
void ResetCapture() {
	bufferIndex = 0;
    totalP = 0;
    captureFull = 0;
    iprintf("\r\n Capture Buffer have been Reset\r\n");
}

/*
 * Function: StartCapture
 * ----------------------
 * begins capturing and storing packages to capBuffer.
 */
void StartCapture() {

	if (captureFull) {
		iprintf("Capture Buffer was full, \r\n");
		ResetCapture();
    }
	iprintf("\r\n Start Capturing \r\n");
	SetCustomNetDoRX(myNetDoRX);
}

/*
 * Function: PauseCapture
 * ----------------------
 * Pause capturing and storing packages to capBuffer.
 */
void PauseCapture() {
	iprintf("\r\n Capturing Paused \r\n");
	ClearCustomNetDoRX();
}

void RegisterPost();

void UserMain(void * pd) {
    init();
    EnablePromiscuous();

    iprintf("Application: %s\r\nNNDK Revision: %s\r\n",AppName,GetReleaseTag());

    // starting ftp service
    iprintf( "Starting FTP Server on port 21 ..." );
    FTPDStart( 21, MAIN_PRIO - 1 );
    iprintf( " complete\r\n" );

    StartHTTP(); // enable webservice

    RegisterPost();

    while ( 1 ) {
        OSTimeDly( TICKS_PER_SECOND );
    }
}

/*
 * Function: SendPcapToClient
 * --------------------------
 * Captured and stored packets are padded with proper
 * file and each packet header to be in pcap format and
 * sent to the client when requested
 *
 * fd : file descriptor to be written
 */
int SendPcapToClient(int fd) {
    // Stops capturing when download request is received
    ClearCustomNetDoRX();

    // write the file header
    writeall(fd, (char*) &header, 24);

    uint32_t nextSize = 0; // index traversing by packet size
    packet* packetToWrite;

    //write total
    iprintf ("\r\n Total packet recorded: %d \r\n", totalP);

    // for each packet stored ...
    for (uint32_t i = 0; i < totalP; i++)
    {
        // index current packet to be written
        packetToWrite = (packet*) (capBuffer + nextSize);

        // populate/write packet header
        writeall(fd, (char*) &packetToWrite->time, 4) ;
        writeall(fd, (char*) &packetToWrite->timeFrac, 4);
        writeall(fd, (char*) &packetToWrite->lenPdata, 4);
        writeall(fd, (char*) &packetToWrite->lenPdata, 4);

        // write packet data
        if(writeall(fd, (char*) packetToWrite->pData, packetToWrite->lenPdata) < 0) { break; }

        // locate index of the next packet to write
        nextSize += packetToWrite->lenPdata + (sizeof(packet));
    }
    return FTPD_OK;
}

static http_gethandler *oldhand;
/*
 * Function: MyDoGet
 * -----------------
 * This function would work similarly to myNetDoRX
 * where it overrides default web browser's GET requests
 * and if it sees that the url contains the READ_FILENAME
 * which is generated by clicking Download button on UI
 * then it calls SendPcapToClient function
 *
 * sock : socket
 * url : current url
 * rxBuffer : not used in this implementation
 */
int MyDoGet (int sock, PSTR url, PSTR rxBuffer) {
    if ( httpstricmp( url, "SAMPLE.PCAP" ) ) {
    	SendPcapToClient(sock);
        return  1;
    }
    return ( *oldhand ) ( sock, url, rxBuffer );
}

/*
 * Function: RegisterPost
 * ----------------------
 * Register our own custom http get handler
 */
void RegisterPost() {
	oldhand = SetNewGetHandler(MyDoGet);
}


/********************************************************************************************
         FILE READ/LIST FUNCTIONS
********************************************************************************************/


/*-----------------------------------------------------------------------
 * Get Directory String
 * This function is used to format a file name into a directory string
 * that the FTP client will recognize. In this very simple case we
 * are just going to hardcode the values.
 *-----------------------------------------------------------------------*/
void getdirstring( char *FileName, long FileSize, char *DirStr )
{
    char tmp[80];

    DirStr[0] = '-';  // '-' for file, 'd' for directory
    DirStr[1] = 0;

    // permissions, hard link, user, group
    strcat( DirStr, "rw-rw-rw- 1 user group " );

    siprintf( tmp, "%9ld ", FileSize );
    strcat( DirStr, tmp );

    strcat( DirStr, "JAN 01 2000 " );

    strcat( DirStr, FileName );
}



/*-----------------------------------------------------------------------
 *  This function is called by the FTP Server in response to a FTP Client's
 *  request to list the files (e.g. the "ls" command)
 *------------------------------------------------------------------------*/
int FTPD_ListFile(const char *current_directory, void *pSession, FTPDCallBackReportFunct *pFunc, int handle)
{
    char DirStr[256];

    // Only one file exists ReadFile.txt
    getdirstring( READ_FILENAME, CAP_BUFF_SIZE, DirStr );
    pFunc( handle, DirStr );
    return FTPD_OK;
}

/*-----------------------------------------------------------------------
 *  This function is called by the FTP Server in response to a FTP
 *  Client's request to receive a file. In this example, only ReadFile.txt
 *  is available for download, and it's contents are hard coded to the
 *  string in the writestring() function.
 *-----------------------------------------------------------------------*/
int FTPD_SendFileToClient(const char *full_directory, const char *file_name, void *pSession, int fd)
{
    // Only one file exists
    if ( strcmp( file_name, READ_FILENAME ) == 0 )
    {
    	return SendPcapToClient (fd);
    }
    else
    {
        return FTPD_FAIL;
    }
}


/*--------------------------------------------------------------------------
 *  This function is called by the FTP Server to determine if a file exists.
 *-------------------------------------------------------------------------*/
int FTPD_FileExists( const char *full_directory, const char *file_name, void *pSession )
{
    // Only one file exists
    if ( strcmp( file_name, READ_FILENAME ) == 0 )
    {
        return FTPD_OK;
    }
    return FTPD_FAIL;
}

/********************************************************************************************
         FILE SEND FUNCTIONS
********************************************************************************************/
/* This example only allows one file to be sent to the board. The file contents are
   sent to the com1 serial port to be displayed; the file is not stored in memory.
   The method you would use to store a file depends on your application. One way
   is to allocate memory and store the file data as an array of bytes. A second way
   is to add a full file system.
*/
char tmp_resultbuff[255];

/* This function reads the data stream from the input stream file descriptor fd
   and displays it to stdout, which is usually the com1 serial port on the
   NetBurner board.
*/
void ShowFileContents( int fdr )
{
    iprintf( "\r\n[" );
    int rv;
    do
    {
        rv = ReadWithTimeout( fdr, tmp_resultbuff, 255, 20 );
        if ( rv < 0 )
        {
            iprintf( "RV = %d\r\n", rv );
        }
        else
        {
            tmp_resultbuff[rv] = 0;
            iprintf( "%s", tmp_resultbuff );
        }
    }
    while ( rv > 0 );
    iprintf( "]\r\n" );
}

/*--------------------------------------------------------------------
 *  This function gets called by the FTP Server when a FTP client
 *  sends a file. File must be named WriteFile.txt.
 *--------------------------------------------------------------------*/
int FTPD_GetFileFromClient(const char *full_directory, const char *file_name, void *pSession, int fd)
{
    if ( strcmp( file_name, "WriteFile.txt" ) == 0 )
    {
        ShowFileContents( fd );
        return FTPD_OK;
    }
    return FTPD_FAIL;
}

/*-------------------------------------------------------------------------
 *  This function gets called by the FTP Server to determine if it is ok to
 *  create a file on the system. In this case is will occur when a FTP
 *  client sends a file. File must be named WriteFile.txt.
 *-------------------------------------------------------------------------*/
int FTPD_AbleToCreateFile(const char *full_directory, const char *file_name, void *pSession)
{
    if ( strcmp( file_name, "WriteFile.txt" ) == 0 )
    {
        return FTPD_OK;
    }
    return FTPD_FAIL;
}

/*-------------------------------------------------------------------
 * The parameters passed to you in this function show the entered
 * user name, password and IP address they came from. You can
 * modify this function any way you wish for authentication.
 *
 * Return Values:
 *   0   = Authentication failed
 *   > 0 = Authentication passed
 * -----------------------------------------------------------------*/
void * FTPDSessionStart( const char *user, const char *passwd, const IPADDR hi_ip )
{
    return ( void * ) 1; //  Return a non zero value
}

/****************FTPD Functions that are not supported/used in this trivial case *******************/
void FTPDSessionEnd( void *pSession )
{
    /* Do nothing */
}

int FTPD_ListSubDirectories(const char *current_directory, void *pSession, FTPDCallBackReportFunct *pFunc, int handle)
{
    /* No directories to list */
    return FTPD_OK;
}

int FTPD_DirectoryExists( const char *full_directory, void *pSession )
{
	if(*full_directory=='\0') {
		return FTPD_OK;
	}
    return FTPD_FAIL;
}

int FTPD_CreateSubDirectory(const char *current_directory, const char *new_dir, void *pSession)
{
    return FTPD_FAIL;
}

int FTPD_DeleteSubDirectory(const char *current_directory, const char *sub_dir, void *pSession)
{
    return FTPD_FAIL;
}

int FTPD_DeleteFile( const char *current_directory, const char *file_name, void *pSession )
{
    return FTPD_FAIL;
}

int FTPD_Rename( const char *current_directory,const char *cur_file_name,const char *new_file_name,void *pSession )
{
    return FTPD_FAIL;
}

int FTPD_Size( const char *full_directory,  const char *file_name, int socket) {
    fdprintf( socket, "213 %ld\r\n", 1 );
    return FTPD_OK;
}

